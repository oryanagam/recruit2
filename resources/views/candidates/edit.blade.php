@extends('layouts.app')
@section('content')
@section('title','Edit Candidate')

       <h1>Edit Candidate</h1>
       <form method = "post" action = "{{action('CandidatesController@update',$candidate->id )}}">
       @method('PATCH')
       @csrf
       <div class = "form-group">
       <label for = "name">Candidate name</label>
       <input type = "text" class = "form-control" name = "name" value = {{$candidate->name }}>
       </div>
       <div class = "form-group">
       <label for = "email">Candidate email</label>
       <input type = "text" class = "form-control" name = "email" value = {{$candidate->email }}>
       </div>
       <div>
       <input type = "submit" name = "submit" value = "Update candidate">
       </div>
       </form>
@endsection