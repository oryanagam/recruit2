@extends('layouts.app')

@section('title', 'Interviews')

@section('content')


                   
<h1>List of Interviews</h1>
<table class = "table table-dark">
<tr> 
      <th>id</th><th>Summery</th><th>User</th><th>Candidate</th><th>Candidate</th<th>Created</th><th>Updated</th>
 </tr>
 @foreach($interviews as $interview)
        <tr>
            <td>{{$interview->id}}</td>
            <td>{{$interview->Summary}}</td>
            <td>
            @if (isset($interview->user_id))
                {{$interview->user->name}}
                @else
                    Undefine user
            @endif
            </td>
            <td>
            @if (isset($interview->candidate_id))
                {{$interview->candidate->name}}
                @else
                    Undefine candidate
            @endif
            </td>
            <td>{{$interview->created_at}}</td>
            <td>{{$interview->updated_at}}</td>
        </tr>
    @endforeach
</table>




@endsection

