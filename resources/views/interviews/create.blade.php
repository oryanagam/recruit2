@extends('layouts.app')
@section('title','Create Interview')
@section('content')

       <h1>Create Interview</h1>
       <form method = "post" action = "{{action('InterviewsController@store')}}">
       @csrf
       <div class = "form-group">
       <label for = "Summary">Interview Summary</label>
       <input type = "text" class = "form-control" name = "Summary">
       </div>
       <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Choose Candidate
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    @foreach($candidates as $candidate)
                    <a class="dropdown-item" href="#">{{$candidate->name}}</a>
                    @endforeach
                    
                    </div>
                  </div> 
                  <p></p>
                  <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Choose User
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    @foreach($users as $user)
                    <a class="dropdown-item" href="#">{{$user->name}}</a>
                    @endforeach
                    
                    </div>
                  </div> 
                  <p></p>
        
       <div>
       <input type = "submit" name = "submit" value = "Create interview">
       </div>
       </form>

@endsection