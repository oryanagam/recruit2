<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interview extends Model
{
    protected $fillable = ['Summary','candidate_id'];

    public function candidate(){
        return $this->belongsTo('App\Candidate' ,'candidate_id');
    }

    public function user(){
        return $this->belongsTo('App\User' ,'user_id');
    }

    //
}
