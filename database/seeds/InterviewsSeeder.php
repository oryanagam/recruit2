<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class InterviewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interviews')->insert([
            'Summary' => Str::random(30),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        //
        ]); 
    }
}
